import getRandomObstacle from '~/utils/Obstacle'
import StateManager from '~/utils/StateManager'

describe('StateManager', () => {
  const size = 200
  const state = new StateManager(size)

  test('State manager mounted without errors', () => {
    expect(state).toBeTruthy()
  })

  test('Map is generated with correct size', () => {
    expect(state.map.length).toBe(size)
  })

  test('Rover is placed in the middle', () => {
    expect(state.rover.getPosition()).toStrictEqual({ x: 100, y: 100 })
  })

  test('Bitacora is initalized', () => {
    expect(state.bitacora.register.length).toBeGreaterThan(0)
  })

  test('Bitacora is initalized', () => {
    expect(state.bitacora.register.length).toBeGreaterThan(0)
  })

  // CHECK ROTATION LEFT, ROVER IS ORIENTED S
  test('CHECKING ROTATION LEFT', async () => {
    const command = 'l'
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('E')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('N')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('W')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('S')
  })

  // CHECK ROTATION LEFT, ROVER IS ORIENTED S
  test('CHECKING ROTATION RIGHT', async () => {
    const command = 'r'
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('W')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('N')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('E')
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('S')
    expect(state.rover.getPosition()).toStrictEqual({ x: 100, y: 100 })
  })

  test('NOT MOVING ON ROTATION', () => {
    expect(state.rover.getPosition()).toStrictEqual({ x: 100, y: 100 })
  })

  test('Bitacora registered all movements', () => {
    expect(state.bitacora.register.length).toBeGreaterThan(8)
  })

  test('MOVING FORWARD IF NOT OBSTACLE', async () => {
    expect(state.rover.getOrientation()).toBe('S')
    const command = 'f'
    delete state.map[state.rover.getY() + 1][state.rover.getX()]
    await state.onInput(command)

    // WE ASSURE THAT NOT IS A OBSTACLE
    expect(state.rover.getPosition()).toStrictEqual({ x: 100, y: 101 })
  })

  test('STOP IF OBSTACLE IN FRONT', async () => {
    // WE PUT A OBSTACLE TO CHECK THE ROVER SENSORS
    const xOfObstacle = 101
    const yOfObstacle = 101
    const obs = getRandomObstacle(xOfObstacle, yOfObstacle)
    state.map[yOfObstacle][xOfObstacle] = {
      background: obs.getBackground(),
      object: obs,
      x: xOfObstacle,
      y: yOfObstacle
    }

    expect(state.rover.getOrientation()).toBe('S')
    const command = 'lf'
    await state.onInput(command)
    expect(state.rover.getOrientation()).toBe('E')
    // WE ARE IN THE SAME SQUARE
    expect(state.rover.getPosition()).toStrictEqual({ x: 100, y: 101 })
  })

  test('IF OBSTACLE OUR STATE IS STOPPED', () => {
    // THE MOVEMENT IS ASYNC SO WE HAVE TO WAIT A LITTLE FOR THE RESPONSE OF ROVER
    setTimeout(() => {
      expect(state.rover.state).toStrictEqual('STOPPED')
    }, 100)
  })

  /* test('Limit works correctly (40)', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)
  })

  test('Remove feature when click in image wrapper', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)

    wrapper.find('.img-wrapper').trigger('click')
    expect(wrapper.vm.imagesDisplayed.length).toBe(49)
  })

  test('Load more images when scrolled to bottom', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)

    window.dispatchEvent(new CustomEvent('scroll', { detail: 2000 }))

    expect(wrapper.vm.imagesDisplayed.length).toBeGreaterThan(50)
  }) */
})

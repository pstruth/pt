
class Bitacora {
    register: [String] = ['Connecting to Mars Rover...']

    constructor (x: number, y: number) {
      this.register.push(`Initial state x: ${x}, y: ${y}`)
    }

    restart (x: number, y: number) {
      this.register = ['Connecting to Mars Rover...']
      this.register.push(`Initial state x: ${x}, y: ${y}`)
    }

    registerMovement (x: number, y: number) {
      this.register.push(`Going forward to: ${x}, y: ${y}`)
    }

    registerRotation (orientation: 'N' | 'E' | 'S' | 'W', x: number, y: number) {
      this.register.push(`Rotation to ${orientation} at x: ${x}, y: ${y}`)
    }

    registerObstacle (x: number, y: number) {
      this.register.push(`Stopped by obstacle at x: ${x}, y: ${y}`)
    }
}

export default Bitacora

/**
 * Class to represent a tiled object in the map
 */
class MarsObject {
    x: number;
    y: number;
    id?: String;

    constructor (x: number, y: number) {
      this.x = x
      this.y = y
    }

    getPosition () {
      return {
        x: this.x,
        y: this.y
      }
    }

    getX () {
      return this.x
    }

    getY () {
      return this.y
    }
}
export default MarsObject

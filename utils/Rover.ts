import MarsObject from './MarsObject'
import { STATES } from './StateManager'

/**
 * Represents the ROVER
 */
class Rover extends MarsObject {
    id = 'Rover'
    orientation: 'N' | 'E' | 'S' | 'W' = 'S'
    degToReturn = 0
    state: STATES = 'READY'

    setOrientation (orientation: 'N' | 'E' | 'S' | 'W' = 'N') {
      const lastDeg = this.degToReturn
      let nDeg = 0
      switch (this.orientation) {
        case 'N':
          if (orientation === 'E') {
            nDeg = 90
          } else if (orientation === 'W') {
            nDeg = -90
          }
          break
        case 'E':
          if (orientation === 'N') {
            nDeg = -90
          } else if (orientation === 'S') {
            nDeg = 90
          }
          break
        case 'S':
          if (orientation === 'E') {
            nDeg = -90
          } else if (orientation === 'W') {
            nDeg = 90
          }
          break
        case 'W':
          if (orientation === 'N') {
            nDeg = 90
          } else if (orientation === 'S') {
            nDeg = -90
          }
          break
      }
      this.degToReturn = lastDeg + nDeg

      this.orientation = orientation
    }

    getOrientation () {
      return this.orientation
    }

    changeOrientation (keyPressed: string) {
      const rotateLeft = keyPressed === 'l' || keyPressed === 'L'
      switch (this.orientation) {
        case 'N':
          this.setOrientation(rotateLeft ? 'W' : 'E')
          break
        case 'E':
          this.setOrientation(rotateLeft ? 'N' : 'S')
          break
        case 'S':
          this.setOrientation(rotateLeft ? 'E' : 'W')
          break
        case 'W':
          this.setOrientation(rotateLeft ? 'S' : 'N')
          break
      }
    }

    getBackground () {
      return '#343A40'
    }

    getNewXwhenForward () {
      switch (this.orientation) {
        case 'N':
          return this.x
        case 'E':
          return this.x + 1
        case 'S':
          return this.x
        case 'W':
          return this.x - 1
      }
    }

    getNewYwhenForward () {
      switch (this.orientation) {
        case 'N':
          return this.y - 1
        case 'E':
          return this.y
        case 'S':
          return this.y + 1
        case 'W':
          return this.y
      }
    }

    getRotationDegrees () {
      let rotation = 0
      switch (this.orientation) {
        case 'N':
          rotation = 180
          break
        case 'E':
          rotation = 90
          break
        case 'S':
          rotation = 180
          break
        case 'W':
          rotation = 270
          break
      }
      // DEG calculados respecto sur
      console.log('rotation', rotation, this.degToReturn)
      return this.degToReturn
    }
}
export default Rover

import Vue from 'vue'
import Bitacora from './Bitacora'
import generateMap, { MarsMAP } from './MapGenerator'
import Rover from './Rover'

export type STATES = 'READY' | 'SEARCHING' | 'STOPPED'

/***
 * CLASS TO MANAGE THE APP
 */
class StateManager {
    frames: number = 50
    size: number
    map: MarsMAP
    rover: Rover
    state: STATES = 'READY'
    obstacle: any = null
    bitacora: Bitacora
    rotateCommands = ['l', 'L', 'r', 'R']
    forwardCommands = ['f', 'F']

    constructor (size: number) {
      this.size = size
      this.map = generateMap(size)
      this.rover = this.generateRover()
      this.bitacora = new Bitacora(this.rover.getX(), this.rover.getY())
    }

    /**
     * RESTART THE POSITION AND GENERATE OTHER MAP
     */
    restart () {
      this.state = 'READY'
      this.obstacle = null
      this.map = generateMap(this.size)
      this.rover.x = this.size / 2
      this.rover.y = this.size / 2
      this.bitacora.restart(this.size / 2, this.size / 2)
    }

    /**
     * GENERATE ROVER IN MIDDLE OF MAP
     * @returns ROVER
     */
    generateRover () {
      const rover = new Rover(this.size / 2, this.size / 2)
      Vue.set(this.map[rover.getY()], rover.getX(), null)
      return rover
    }

    /**
     * PROCESS COMMANDS
     * @param command STRING OF COMMANDS (L,l,F,f,R,r)
     * @returns nothing
     */
    async onInput (command: string) {
      this.state = 'SEARCHING'
      for (let i = 0; i < command.length; i++) {
        const nkey = command.charAt(i)

        const objc = await this.keyDown(this.frames, nkey)
        if (!objc.success) {
          this.state = 'STOPPED'
          this.obstacle = objc.obstacle
          return
        }
      }
      this.state = 'READY'
    }

    /**
     * CHECKS FOR OBSTACLE
     * @param x X OF NEW SQUARE
     * @param y Y OF NEW SQUARE
     * @returns IF OBSTACLE
     */
    checkObstacle (x: number, y: number) {
      const tile = this.map[y][x]
      if (tile && tile.object) {
        return tile.object
      }
    }

    /**
     * PROCESS A SINGLE CMMAND
     * @param frames FRAMES TO WAIT FOR NEW ANIMATION
     * @param key COMMAND
     * @returns PROMISE IF ALL OK OR OBSTACLE FOUND
     */
    keyDown (frames: number, key: string) : Promise<{success: boolean, obstacle: any}> {
      return new Promise((resolve) => {
        setTimeout(() => {
          Vue.nextTick(() => {
            if (this.rotateCommands.includes(key)) {
              this.rover.changeOrientation(key)
              /*  Vue.set(this.map[this.rover.getY()], this.rover.getX(), {
                ...this.map[this.rover.getY()][this.rover.getX()],
                object: this.rover
              }) */
              this.bitacora.registerRotation(this.rover.getOrientation(), this.rover.getX(), this.rover.getY())
              resolve({ success: true, obstacle: null })
              return
            }
            if (this.forwardCommands.includes(key)) {
              const newX = this.rover.getNewXwhenForward()
              const newY = this.rover.getNewYwhenForward()

              const obstacle = this.checkObstacle(newX, newY)
              if (obstacle) {
                console.log('OBSTACULO')
                this.state = 'STOPPED'
                this.bitacora.registerObstacle(newX, newY)
                resolve({ success: false, obstacle })
                return
              }

              this.bitacora.registerMovement(newX, newY)
              this.rover.y = newY
              this.rover.x = newX

              /*    Vue.set(this.map[this.rover.getY()], this.rover.getX(), {
                background: this.rover.getBackground(),
                object: this.rover,
                y: this.rover.getY(),
                x: this.rover.getX()
              })
              Vue.set(this.map[originaly], originalx, null) */
              resolve({ success: true, obstacle: null })
            }
          })
        }, frames)
      })
    }
}

export default StateManager

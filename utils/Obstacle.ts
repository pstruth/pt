import MarsObject from './MarsObject'

export type OBSTACLETYPE = 'STONE' | 'HOLE' | 'MARTIAN'
/**
 * Represents a obstacle
 */
class Obstacle extends MarsObject {
    typeObstacle: OBSTACLETYPE

    constructor (type: OBSTACLETYPE, x: number, y: number) {
      super(x, y)
      this.typeObstacle = type
    }

    getBackground () {
      switch (this.typeObstacle) {
        case 'STONE':
          return '#DBD0C0'
        case 'HOLE':
          return '#000000'
        case 'MARTIAN':
          return '#B1E693'
      }
    }

    getDescriptionText () {
      switch (this.typeObstacle) {
        case 'STONE':
          return 'FOUND A BIG STONE'
        case 'HOLE':
          return 'FOUND A SUPER BIG HOLE'
        case 'MARTIAN':
          return 'FOUND A GREEN MARTIAN?'
      }
    }

    getDescriptionImage () {
      switch (this.typeObstacle) {
        case 'STONE':
          return 'stone.jpg'
        case 'HOLE':
          return 'hole.jpg'
        case 'MARTIAN':
          return 'martian.jpg'
      }
    }
}

const getRandomObstacle = (x: number, y: number) => {
  let type: OBSTACLETYPE = 'STONE'
  const rIndex = Math.floor(Math.random() * 6)
  if (rIndex > 4) {
    type = 'MARTIAN'
  } else if (rIndex > 2) {
    type = 'HOLE'
  }
  return new Obstacle(type, x, y)
}
export default getRandomObstacle

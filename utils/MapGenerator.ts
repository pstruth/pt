import getRandomObstacle from './Obstacle'

export type TileSquare = {
    background: string,
    object: any,
    x: number,
    y: number
}

export type MarsMAP = Array<Array<TileSquare>>

const generateMap = (size: number) => {
  const newArray = new Array<Array<TileSquare>>(size)

  for (let i = 0; i < newArray.length; i++) {
    newArray[i] = new Array<TileSquare>(size)
    fillMap(newArray[i], i)
  }

  return newArray
}

const fillMap = (array: Array<TileSquare>, y: number) => {
  for (let index = 0; index < array.length; index++) {
    const haveObstacle = Math.random() > 0.9
    if (haveObstacle) {
      const obs = getRandomObstacle(index, y)
      array[index] = {
        background: obs.getBackground(),
        object: obs,
        x: index,
        y
      }
    }
  }
}

export default generateMap

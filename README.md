# Prueba técnica por Pablo Struth

## MARS ROVER.

La versión mobile no cuenta con el soporte visual que sí cuenta la versión desktop dónde se puede ver un mapa del mundo y una pequeña animación del rover según los comandos introducidos. Para mayor cómodidad he dejado un comando por defecto introducido. El mapa de Marte se genera de forma aleatoria. Hay hasta tres tipos de obstáculos.

### `Misión`

```bash
You’re part of the team that explores Mars by sending remotely controlled vehicles to the surface
of the planet. Develop a software that translates the commands sent from earth to instructions
that are understood by the rover.
```

### `Requerimientos`

```bash
● You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W)
it is facing.
● The rover receives a collection of commands. (E.g.) FFRRFFFRL
● The rover can move forward (f).
● The rover can move left/right (l,r).
● Suppose we are on a really weird planet that is square. 200x200 for example :)
● Implement obstacle detection before each move to a new square. If a given
sequence of commands encounters an obstacle, the rover moves up to the last
possible point, aborts the sequence and reports the obstacle.
```

## Correr tests y la aplicación

Con Nuxt podemos crear tanto una versión para levantarla en un puerto (por defecto el 3000) como generar un directorio estático. En este caso dejo la instrucción para levantarla en un puerto que para probar es lo más sencillo.

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

Para correr los tests bastará con ejecutar:
```bash
# install dependencies (en caso de no haberlo hecho)
$ npm install

# serve with hot reload at localhost:3000
$ npm run test
```


